extends Node

class_name LevelHandler
""" class LevelHandler, 
handles how levels are loaded. """

export var levels: PoolStringArray
var _curr: int


func set_curr(curr: int) -> void:
	""" set current level's index """
	if curr < len(self.levels):
		self._curr = curr
	else:
		self._curr = 0


func get_curr() -> int:
	""" get current level's index """
	return self._curr


func level_instance() -> void:
	var lvl : Node =  load(self.levels[self._curr]).instance()
	self.add_child(lvl)


func level_finished():
	self.remove_child(get_node(self.levels[self.get_curr()]))
	self.set_curr(self.get_curr() + 1)

